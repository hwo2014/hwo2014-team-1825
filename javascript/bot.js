var net = require('net');
var JSONStream = require('JSONStream');

var Track = require('./track');

var Bot = function(serverPort, serverHost, botName, botKey){
    
    var bot = this;
    
    // creds
    bot.serverPort = serverPort;
    bot.serverHost = serverHost;
    bot.botName = botName;
    bot.botKey = botKey;
    
    // network
    bot.client = null;
    bot.clientPipe = null;
    
    // info
    bot.joined = false;
    bot.raceStarted = false;
    bot.gameTick = -1;
    
    // properties
    bot.track = null;
    bot.speed = null;
    bot.throttle = null;
    bot.throttles = [];
    bot.avgThrottle = 0;
    bot.angle = null;
    bot.color = null;
    bot.position = null;
    bot.lane = null;
    bot.changingLane = 0;
    
    // last pos
    bot.lastPiece = -1;
    bot.lastDistance = -1;
    
    // stats
    bot.topSpeed = 0;
    bot.avgSpeed = 0;
    bot.speedRecordings = [];
    bot.crashes = 0;
    bot.lapTimes = [];
    
};

Bot.prototype.init = function(){

    var bot = this;
    
    bot.log('Initializing bot '+bot.botName);
    
    // initialise network
    bot.client = net.connect(bot.serverPort, bot.serverHost, function() {
        // init network join
        bot.join();
    });
    bot.clientPipe = bot.client.pipe(JSONStream.parse());
    bot.clientPipe.on('data', function(data) {
        bot.handleMessage(data);
    });
    bot.clientPipe.on('error', function() {
        return console.log("disconnected");
    });
    
    bot.track = new Track();
    
};

Bot.prototype.log = function(message){
    
    var bot = this;
    console.log(bot.gameTick + ' > ' + message);
    
};

Bot.prototype.send = function(message){
    
    var bot = this;
    
    //bot.log(message.msgType+' > '+JSON.stringify(message.data));
    
    bot.client.write(JSON.stringify(message));
    return bot.client.write('\n');
    
};

Bot.prototype.handleMessage = function(message){
    
    var bot = this;
    
    bot.gameTick = message.gameTick ? message.gameTick : bot.gameTick;
    
    switch(message.msgType){
        
        case 'yourCar':         bot.handleCar(message.data);            break;
        case 'gameInit':        bot.handleGameInit(message.data);       break;
        case 'gameStart':       bot.handleGameStart();                  break;
        case 'carPositions':    bot.handlePositions(message.data);      break;
        case 'gameEnd':         bot.handleGameEnd(message.data);        break;
        case 'crash':           bot.handleCrash(message.data);          break;
        case 'lapFinished':     bot.handleLapFinished(message.data);    break;
        
    }
    
};

Bot.prototype.join = function(){
    
    var bot = this;
    
    bot.log('Joining session...');
    
    bot.send({
        msgType: 'join',
        data: {
            name: bot.botName,
            key: bot.botKey
        },
        gameTick: bot.gameTick+1
    });
    
};

Bot.prototype.handleCar = function(data){
    
    var bot = this;
    
    bot.color = data.color;
    
    bot.log('Car color: '+bot.color);
    
};

Bot.prototype.handleGameInit = function(data){
    
    var bot = this;
    
    // set track details
    var track = bot.track;
    
    track.id = data.race.track.id;
    track.name = data.race.track.name;
    track.laps = data.race.raceSession.laps;
    track.maxLapTimeMs = data.race.raceSession.maxLapTimeMs;
    track.quickRace = data.race.raceSession.quickRace;
    
    bot.log('');
    bot.log('==== Track details ====');
    bot.log('  name:       '+track.name);
    bot.log('  id:         '+track.id);
    bot.log('  no laps:    '+track.laps);
    bot.log('  max time:   '+track.maxLapTimeMs);
    bot.log('  quick race: '+track.quickRace);
    
    // add pieces & lanes
    track.lanes = data.race.track.lanes;
    track.pieces = data.race.track.pieces;
    track.parsePieces();

    bot.log('  length:     '+track.pieces.length);
    bot.log('  lanes:      '+track.lanes.length);
    bot.log('');
    
};

Bot.prototype.handleGameStart = function(){
    
    var bot = this;
    
    bot.raceStarted = true;
    bot.throttle = 0.65;
    
    bot.log('#######################');
    bot.log('#    RACE  STARTED    #');
    bot.log('#######################');
    bot.log('');
    
};

Bot.prototype.handlePositions = function(data){
    
    var bot = this;
    
    var c=0, car, cars = data, num_cars = cars.length;
    
    for(c;c<num_cars;c++){
        car = cars[c];
        if(car.id.color === bot.color && car.id.name === bot.botName){
            // set position
            bot.position = c;
            // set thottle
            if(bot.gameTick > 0) bot.setThrottle(car);
            break;
        }
    }
    bot.send({
      msgType: "ping",
      data: {}
    });
};

Bot.prototype.setThrottle = function(carData){
    
    var bot = this;

    // set lane
    var 
        first = false,
        lane = carData.piecePosition.lane.startLaneIndex, newPiece = false;

    if(bot.lastPiece === -1) first = true;

    if(lane !== bot.lane){
        bot.changingLane = 0;
        bot.lane = lane;
    }
    if(bot.lastPiece !== carData.piecePosition.pieceIndex){
        bot.lastPiece = carData.piecePosition.pieceIndex;
        newPiece = bot.track.getPiece(bot.lastPiece);
        //console.log(newPiece.straight? 'Straight' : newPiece.clockwise ? 'Right' : 'Left', newPiece.crossover ? 'Crossover' : '');
        newPiece = true;
    }
    
    var throttle, thisPieceID = carData.piecePosition.pieceIndex,
        distance = carData.piecePosition.inPieceDistance;

    if(distance === 0){
        distance++;
    }
    /*
    var thisPiece = bot.track.getPiece(thisPieceID),
        nextPiece = bot.track.getPiece(thisPieceID + 1);

    var throttle = (thisPiece.length[bot.lane] / distance) * ( thisPiece.throttle - nextPiece.throttle ) + nextPiece.throttle;
    
    throttle = throttle > 1 ? 1 : throttle < 0 ? 0 : throttle;
    
    //bot.log('Send Throttle: '+throttle);
    */
   
    // lets try and work out which lane to be in
    // lookahead
    if(bot.changingLane === 0 && newPiece && !first){
        var ahead = bot.track.lookAhead(bot.lastPiece, 10),
            desiredLane = '',
            hasSwitch = false,
            
            piece = 0,
            ahead_len = ahead.length,
            corners = false, 
            straight = 0;
    
        for(piece; piece<ahead_len; piece++){
            if(!corners && ahead[piece].straight === true) straight++;
            if(ahead[piece].crossover){
                hasSwitch = true;
            }
            if(ahead[piece].clockwise === true){
                corners = true;
                if(hasSwitch && desiredLane === '') desiredLane = 'Right';
            }else if(ahead[piece].anticlockwise === true){
                corners = true;
                if(hasSwitch && desiredLane === '') desiredLane = 'Left';
            }
        }
        
        if(straight >= 3){
            bot.throttle = 0.79;
            bot.log('Throttle up! (straight: '+straight+')');
        }else if(straight < 3 && bot.avgThrottle > 0.68){
            bot.throttle = 0.67;
            bot.log('Throttle down! Angle: '+carData.angle+', Avg: '+bot.avgThrottle+', straight: '+straight+', throttle: '+bot.throttle);
        }else{
            bot.throttle = 0.68;
            bot.log('Throttle normal! Angle: '+carData.angle+', Avg: '+bot.avgThrottle+', straight: '+straight+', throttle: '+bot.throttle + ', piece: '+carData.piecePosition.pieceIndex + ', straight: '+bot.track.getPiece(carData.piecePosition.pieceIndex).straight);
        }
        //bot.log('Throttle: '+bot.throttle+', avgThrottle: '+bot.avgThrottle+', straights: '+straight);
        
        // check if can change lane that way
        var lane_count = bot.track.lanes.length,
            curr_lane = bot.lane;
            
        if(desiredLane !== '' && hasSwitch){
            
            if(
                desiredLane === 'Right' && curr_lane < lane_count-1
             || desiredLane === 'Left' && curr_lane > 0
            ){
        
                bot.log('Piece '+bot.lastPiece+' has a crossover ahead, move '+desiredLane);
                
                // send change lane
                bot.send({
                    msgType:'switchLane',
                    data: desiredLane
                });
                bot.changingLane = 1;
            }
        }
    }
    /*
    if(bot.changingLane === 1 && bot.track.getPiece(carData.piecePosition.pieceIndex).crossover){
        bot.throttle = 0;
    }
   
    if(carData.angle !== 0){
        bot.throttle = 0; //1 - (Math.abs(carData.angle) / 90);
    }
    */
    // add avgThrottle for last 5
    bot.setAvgThrottle(bot.throttle);
    
    //bot.log('Send Throttle: '+bot.throttle);
    bot.send({"msgType": "throttle", "data": bot.throttle, gameTick: bot.gameTick});
    
};

Bot.prototype.setAvgThrottle = function(newThrottle){
    
    var bot = this;

    var min_avg = 10;

    if(bot.throttles.length >= min_avg){
        bot.throttles.shift();
    }
    
    bot.throttles.push(newThrottle);
    
    
    var t, throttle=0, throttles = bot.throttles, throttle_count = throttles.length;
    if(throttle_count === min_avg){
        for(t=0;t<throttle_count;t++){
            throttle += throttles[t];
        }
        bot.avgThrottle = throttle / throttle_count;
    }
    
};

Bot.prototype.handleGameEnd = function(data){
    
    var bot = this;

    bot.log('');
    bot.log('#######################');
    bot.log('#    RACE FINISHED    #'); 
    bot.log('#######################');
    bot.log('');
    
    // calculate average speed;
    var s, speed, recordings = bot.speedRecordings, avg = 0, count = bot.speedRecordings.length, time;
    
    for(s=0;s<count;s++){
        speed = recordings[s];
        avg += speed;
    }
    avg = avg / count;
    //bot.log(' Top speed:     ' + bot.topSpeed);
    //bot.log(' Average Speed: ' + avg);
    bot.log(' No. Crashes:   ' + bot.crashes);
    
    count = bot.lapTimes.length;
    recordings = bot.lapTimes;
    avg = 0;
    
    var fastest = -1;
    
    for(s=0;s<count;s++){
        time = recordings[s];
        avg += time.millis;
        if(fastest === -1 || fastest > time.millis){
            fastest = time.millis;
        }
    }
    avg = avg / count;
    bot.log(' Fastest lap:   ' + (fastest/1000).toFixed(2)+'ms');
    bot.log(' Average lap:   ' + (avg/1000).toFixed(2)+'ms');
    bot.log('~~~~~~~~~~~~~~~~~~~~~~~');
    for(s=0;s<count;s++){
        bot.log(' Lap '+(s+1)+': '+(recordings[s].millis/1000).toFixed(2)+'ms');
    }
    
};

Bot.prototype.handleCrash = function(data){
    
    var bot = this;
    
    bot.crashes++;
    var piece = bot.track.getPiece(bot.lastPiece);
    bot.log('CRASHED! on '+(piece.straight?'straight':'corner')+' '+bot.lastPiece+' running at '+bot.throttle+' ('+bot.avgThrottle+' Avg).');
    
};

Bot.prototype.handleLapFinished = function(data){
    
    var bot = this;
    bot.log('Lap '+(bot.lapTimes.length+1)+' finished: '+(data.lapTime.millis/1000).toFixed(2)+'s');
    if(data.car.color === bot.color){
        bot.lapTimes.push(data.lapTime);
    }
    
};

module.exports = Bot;

