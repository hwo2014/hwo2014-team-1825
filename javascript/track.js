

var Track = function(){
    
    var track = this;
    
    // properties
    track.id = null;
    track.name = null;
    track.laps = -1;
    track.maxLapTimeMs = -1;
    track.quickrace = null;
    
    // pieces
    track.pieces = [];
    
    // lanes
    track.lanes = [];
    
};

Track.prototype.getPiece = function(index){
    
    var track = this;
    
    if(index < 0){
        index = track.pieces.length + index;
    }
    
    return track.pieces[index % track.pieces.length];
    
};

Track.prototype.parsePieces = function(){
    
    var track = this;
    
    var pieces = track.pieces, num_pieces = pieces.length, p, piece,
        lanes = track.lanes, num_lanes = lanes.length, l, lane, tempLen;
    
    for(p=0; p<num_pieces; p++){
        
        piece = pieces[p];
        
        if(piece['switch'] === true){
            piece.crossover = true;
        }else{
            piece.crossover = false;
        }
        
        if(piece.radius && piece.angle){
            // calculate length
            // piece.length = (piece.angle / 360) * 2 * Math.PI * piece.radius;
            piece.length = [];
            // work out for all lanes
            for(l=0;l<num_lanes;l++){
                lane = lanes[l];
                piece.length[l] = (piece.angle / 360) * 2 * Math.PI * (piece.radius + lane.distanceFromCenter);
            }
            piece.straight = false;
            if(piece.angle>0){
                piece.clockwise = true;
            }else{
                piece.anticlockwise = true;
            }
        }else{
            tempLen = piece.length;
            piece.length = [];
            piece.straight = true;
            for(l=0;l<num_lanes;l++){
                piece.length[l] = tempLen;
            }
        }
        
    }
    
    /*
    
    // 
    // go through and estimate speed:
    // 
    //      straight - straight - straight  1
    //      straight - straight - corner    0.8
    //      straight - corner   - x         ?
    //          [straight] - straight - corner - x   0
    //          [corner]   - straight - corner - x   0.6
    //      corner   - straight - x         0.4
    //      corner   - corner   - x         0.2
    //
    
    pieces = track.pieces;

    var tpiece;

    for(p=0; p<num_pieces; p++){
        
        piece = pieces[p];
        
        if(piece.straight){
            
            if(pieces[(p+1) % num_pieces].straight){
                
                if(pieces[(p+2) % num_pieces].straight){
                    
                    piece.throttle = 1;
                    
                }else{
                    
                    piece.throttle = 0.8;
                    
                }
                
            }else{
                
                tpiece = p - 1;
                if(tpiece === -1){
                    tpiece = num_pieces - 1;
                }
                
                if(pieces[tpiece].straight){
                    piece.throttle = 0.6;
                }else{
                    piece.throttle = 0;
                }
                
            }
            
        }else{
            
            if(pieces[(p+1) % num_pieces].straight){
                
                piece.throttle = 0.4;
                
            }else{
                
                tpiece = p - 1;
                if(tpiece === -1){
                    tpiece = num_pieces - 1;
                }
                
                piece.throttle = 0.2;
                
                pieces[tpiece].throttle -= 0.2;
                if(pieces[tpiece].throttle < 0.1) pieces[tpiece].throttle = 0.1;
                
            }
            
        }

        tpiece = p - 1;
        if(tpiece === -1){
            tpiece = num_pieces - 1;
        }
        pieces[tpiece].nextThrottle = piece.throttle;
        
    }
    */
};

Track.prototype.lookAhead = function(pieceID, ahead){
    
    var track = this;
    
    var i=pieceID, count=pieceID+ahead, piece, pieces = track.pieces, piece_count = pieces.length, ret = [];
    // return the next 4 pieces
    for(i; i<count; i++){
        piece = pieces[i % piece_count];
        ret.push({
            straight: piece.straight,
            crossover: piece.crossover,
            clockwise: piece.clockwise,
            anticlockwise: piece.anticlockwise
        });
    }
    return ret;
};

module.exports = Track;
